﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb_Administrar
{
    public partial class w_seg_AdministracionEmpresa : System.Web.UI.Page
    {
        protected cls_seg_Empresa objEmpresa = new cls_seg_Empresa();
        protected cls_seg_Encriptacion objEncrypt = new cls_seg_Encriptacion();
        protected clsError objError = new clsError();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["gs_UsuLog"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
                f_BindGrid_Inicial(); //grvEmpresa
                //OBTENER PALCLA
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsPalabraClave.Select(args);
                DataTable dt = view.ToTable();

                Session.Add("gs_PalCla", dt.Rows[0]["palcla"]);
            }
        }
        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            f_limpiarCampos();
        }
        protected void btnAbrir_Click(object sender, EventArgs e)
        {

        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            lblExito.Text = "";
            lblError.Text = "";
            if (String.IsNullOrEmpty(txtRucemp.Text) || String.IsNullOrEmpty(txtNomdbm.Text) || String.IsNullOrEmpty(txtNombas.Text) || String.IsNullOrEmpty(txtClabas.Text) || String.IsNullOrEmpty(txtUsubas.Text) || String.IsNullOrEmpty(txtNomemp.Text) || String.IsNullOrEmpty(txtIpserv.Text) || String.IsNullOrEmpty(txtEspdis.Text) || String.IsNullOrEmpty(txtNumlic.Text) || String.IsNullOrEmpty(txtNumlic.Text) ||
                String.IsNullOrEmpty(txtSerlic.Text) || String.IsNullOrEmpty(txtCodac1.Text) || String.IsNullOrEmpty(txtCodac2.Text) || String.IsNullOrEmpty(txtCodac3.Text) || String.IsNullOrEmpty(txtCodac4.Text))
            {
                lblError.Text = "Debe llenar todos los campos para guardar. ";
            }
            else
            {

                objEmpresa.nomdbm = txtNomdbm.Text;
                objEmpresa.nombas = txtNombas.Text;
                objEmpresa.clabas = txtClabas.Text;
                objEmpresa.usubas = txtUsubas.Text;
                objEmpresa.rucemp = txtRucemp.Text;
                objEmpresa.ipserv = txtIpserv.Text;
                objEmpresa.nomemp = txtNomemp.Text;
                objEmpresa.estcad = ddlEstcad.SelectedValue;

                try
                {
                    objEmpresa.feccad = Convert.ToDateTime(txtFeccad.Value);
                }
                catch (Exception ex) //si tiene fecnac en NULL que no sea readonly
                {
                    objEmpresa.feccad = DateTime.Now.Date;
                }

                try
                {
                    objEmpresa.espdis = Convert.ToDecimal(txtEspdis.Text);
                }
                catch (Exception ex) //si tiene fecnac en NULL que no sea readonly
                {
                    objEmpresa.espdis = 0;
                }

                objEmpresa.numlic = txtNumlic.Text;
                objEmpresa.serlic = txtSerlic.Text;
                objEmpresa.codac1 = txtCodac1.Text;
                objEmpresa.codac2 = txtCodac1.Text;
                objEmpresa.codac3 = txtCodac1.Text;
                objEmpresa.codac4 = txtCodac1.Text;
                string s_PalCla = objEncrypt.f_seg_Encriptar(Session["gs_PalCla"].ToString());

                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsEmpresaExiste.Select(args);

                if (view != null) //ya existe el empresa (Actualizar)
                {

                    objError = objEmpresa.f_Empresa_CRUD(objEmpresa, "ADMIN", "ADMIN", "Update");

                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {

                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            Session["gs_Rucemp"] = objEmpresa.rucemp;
                            lblError.Text = "";
                            lblExito.Text = "✔️ Empresa Modificada.";
                            f_BindGrid_Inicial();
                        }
                        else
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }


                }
                else //no existe el empresa (Insertar)
                {
                    if (String.IsNullOrEmpty(objEmpresa.rucemp)) { }
                    else
                    {
                        objError = objEmpresa.f_Empresa_CRUD(objEmpresa, "ADMIN", "ADMIN", "Add");
                    }

                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {

                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            Session["gs_Rucemp"] = objEmpresa.rucemp;
                            lblError.Text = "";
                            lblExito.Text = "✔️ Empresa Creada.";
                            f_BindGrid_Inicial();
                        }
                        else
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else
                    {
                        f_ErrorNuevo(objError);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                    }
                }

                //VALIDA AUDITORIA

                objEmpresa.f_Empresa_Auditoria(objEmpresa.rucemp, "ADMIN", DateTime.Now, "ADMIN", DateTime.Now);
                objError = objEmpresa.f_Empresa_Encriptar_Base(objEmpresa, s_PalCla);
                if (String.IsNullOrEmpty(objError.Mensaje))
                {
                    lblExito.Text = lblExito.Text + " - Datos de Conexión Registrados Correctamente.";
                }
                else
                {
                    f_ErrorNuevo(objError);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            lblExito.Text = "";
            lblError.Text = "";
            f_limpiarCampos();
        }
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_Rucemp"].ToString()))
            {
                Session["gs_Rucemp"] = txtRucemp.Text;
                lblEliEmp.Text = Session["gs_Rucemp"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un cliente para eliminar.";
            }
        }
        protected void btnEliminarEmpresa_Click(object sender, EventArgs e)
        {

            clsError objError = objEmpresa.f_Empresa_Eliminar(Session["gs_Rucemp"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                f_limpiarCampos();
                f_BindGrid_Inicial(); //grvEmpresa
                lblExito.Text = "✔️ Empresa Eliminada.";
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        protected void lkbtnPrincipio_Click(object sender, EventArgs e)
        {

        }
        protected void lkbtnSiguiente_Click(object sender, EventArgs e)
        {

        }
        protected void lkbtnFinal_Click(object sender, EventArgs e)
        {

        }
        protected void lkbtnAtras_Click(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static cls_seg_Empresa f_Validar(string s_codpro)  //VALIDA y OBTIENE datos al hacer tab en txtCodpro en w_Inicio
        {
            cls_seg_Empresa objEmpresa = new cls_seg_Empresa();
            objEmpresa.Codpro = s_codpro;
            objEmpresa.RucCed = s_codpro;

            if (s_codpro.Length == 10) //si es CEDULA
            {
                objEmpresa.TipInd = "2";
                objEmpresa.TipEmp = "3"; // NO ESTABA EN VALIDACION ORIGINAL
                if (s_codpro.Substring(2, 1).Equals("0") || s_codpro.Substring(2, 1).Equals("1") ||
                     s_codpro.Substring(2, 1).Equals("2") || s_codpro.Substring(2, 1).Equals("3") ||
                     s_codpro.Substring(2, 1).Equals("4") || s_codpro.Substring(2, 1).Equals("5"))
                {
                    objEmpresa.TipEmp = "3";
                }
                else
                {
                    objEmpresa.TipEmp = "-"; //para mandarle al error en f_ValidarCedula
                    objEmpresa.Error1 = "3er digito de cédula incorrecto.";
                }
            }
            else
            {
                if (s_codpro.Length == 13) //si es RUC
                {
                    objEmpresa.TipInd = "1";
                    if (s_codpro.Substring(2, 1).Equals("9"))
                    {
                        objEmpresa.TipEmp = "1";
                    }
                    else
                    {
                        if (s_codpro.Substring(2, 1).Equals("6"))
                        {
                            objEmpresa.TipEmp = "2";
                        }
                        else
                        {
                            objEmpresa.TipEmp = "3";
                        }
                    }
                }
                else
                {
                    if (s_codpro.Length == 9) //si es PASAPORTE
                    {
                        objEmpresa.TipInd = "3";
                        objEmpresa.TipEmp = "3";
                    }
                    else //ERROR, NO ES NI CEDULA NI RUC NI PASAPORTE (tiene digitos de mas o de menos)
                    {
                        objEmpresa.TipInd = "-";
                        objEmpresa.TipEmp = "-";
                        objEmpresa.RucCed = "-";
                        objEmpresa.Error1 = "Documento no válido";
                        return objEmpresa;
                    }
                }
            }

            //validacion
            string s_tipemp = objEmpresa.TipEmp.ToString();
            string s_tipind = objEmpresa.TipInd.ToString();
            w_seg_AdministracionEmpresa pg_Inicio = new w_seg_AdministracionEmpresa(); //para poder llamar a Session en WebMethod
            pg_Inicio.Session["gs_Codpro"] = s_codpro;
            pg_Inicio.Session["gs_Tipind"] = s_tipind; //para llenar on el # en tblPrpveedores
            pg_Inicio.Session["gs_Tipemp"] = s_tipemp; //para llenar on el # en tblPrpveedores
            objEmpresa.TipEmp = objEmpresa.f_DeterminarTipEmp(s_tipemp);
            objEmpresa.TipInd = objEmpresa.f_DeterminarTipInd(s_tipind);

            string s_error = objEmpresa.f_ValidacionCedula(s_codpro, s_tipemp);
            if (!s_error.Equals("CORRECTO"))
            {
                objEmpresa.Error1 = s_error;
                objEmpresa.TipInd = "-";
                objEmpresa.TipEmp = "-";
                objEmpresa.RucCed = "-";
                return objEmpresa;
            }
            if (s_tipind.Equals("1")) //siz es RUC
            {
                clsRootObjectRUC objRUC = objEmpresa.f_ObtenerDatosRUC(s_codpro);
                if (objRUC.success == true)
                {
                    List<clsRUC> listRoot = objRUC.result;
                    //w_Inicio pg_Inicio = new w_Inicio(); //para poder llamar a Session en WebMethod
                    pg_Inicio.Session["gs_Nompro"] = listRoot[0].razonSocial;
                    objEmpresa.Nompro = listRoot[0].razonSocial;
                    //pg_Inicio.Session["gs_Codgeo"] = listRoot[0].DPA_NombreProvincia; //OJO DESCOMENTAR
                    pg_Inicio.Session["gs_Nomciu"] = listRoot[0].DPA_NombreCanton;
                    pg_Inicio.Session["gs_Dirpro"] = listRoot[0].direccionContribuyente;
                }
            }
            if (s_tipind.Equals("2")) //si es CEDULA
            {
                clsRootObjectCedula objCED = objEmpresa.f_ObtenerDatosCED(s_codpro);
                if (objCED.success == true)
                {
                    clsCedula listRoot = objCED.result;
                    //w_Inicio pg_Inicio = new w_Inicio(); //para poder llamar a Session en WebMethod
                    pg_Inicio.Session["gs_Nompro"] = listRoot.NombreCiudadano;
                    objEmpresa.Nompro = listRoot.NombreCiudadano;
                    //pg_Inicio.Session["gs_Codgeo"] = listRoot.DPA_ProvinDomicilio; //OJO DESCOMENTAR
                    pg_Inicio.Session["gs_Nomciu"] = listRoot.DPA_CantonDomicilio;
                    pg_Inicio.Session["gs_Dirpro"] = listRoot.CallesDomicilio;
                }
            }

            return objEmpresa;
        }

        protected void ddlEstcad_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExito.Text = "";
            lblError.Text = "";
            if (ddlEstcad.SelectedValue == "2")
            {
                txtFeccad.Attributes.Remove("readonly");
            }
            else
            {
                txtFeccad.Attributes.Add("readonly", "readonly");
            }

        }

        protected void grvEmpresa_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;

            }
        }
        protected void grvEmpresa_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {

            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }



        }
        protected void grvEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_limpiarCampos();
            GridViewRow fila = grvEmpresa.SelectedRow;
            string s_rucemp = fila.Cells[2].Text.Trim();

            Session["gs_Rucemp"] = s_rucemp; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            objEmpresa.rucemp = s_rucemp;
            cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
            string s_PalCla = objEncriptacion.f_seg_Encriptar(Session["gs_PalCla"].ToString());

            objEmpresa = objEmpresa.f_Empresa_Desencriptar_Base(objEmpresa, s_PalCla);
            if (String.IsNullOrEmpty(objEmpresa.Error.Mensaje))
            {
                f_llenarCamposEmpresaBase(objEmpresa);
                objEmpresa = objEmpresa.f_Empresa_Buscar(s_rucemp);
                if (String.IsNullOrEmpty(objEmpresa.Error.Mensaje))
                {
                    f_llenarCamposEmpresa(objEmpresa);

                }
                else
                {
                    f_ErrorNuevo(objEmpresa.Error);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                }

            }
            else
            {
                f_ErrorNuevo(objEmpresa.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsEmpresa.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Empresa", dt);

            grvEmpresa.DataSource = dt;
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Empresa"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "rucemp LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nomemp LIKE " + "'%" + searchText.Trim();
                    grvEmpresa.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvEmpresa.DataSource = dv;
                }
                grvEmpresa.DataBind();
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchEmpresa.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalEmpresa();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvEmpresa.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchEmpresa.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchEmpresa.Text);
        }
        public void f_llenarCamposEmpresa(cls_seg_Empresa objEmpresa)
        {
            txtRucemp.Text = objEmpresa.rucemp;
            txtRucemp.Text = objEmpresa.rucemp;
            txtNomemp.Text = objEmpresa.nomemp;
            ddlEstcad.SelectedValue = objEmpresa.estcad;

            DateTime s_feccad = objEmpresa.feccad;
            try
            {

                string s_Fecfinal = s_feccad.ToString("yyyy-MM-dd");
                txtFeccad.Value = s_Fecfinal;
                txtEspdis.Text = Convert.ToString(objEmpresa.espdis);
            }
            catch (Exception ex) //si tiene fecnac en NULL que no sea readonly
            {
                txtFeccad.Value = DateTime.Now.Date.ToString();
                txtEspdis.Text = "0";
                //throw;
            }


            txtNumlic.Text = objEmpresa.numlic;

            txtSerlic.Text = objEmpresa.serlic;
            txtCodac1.Text = objEmpresa.codac1;
            txtCodac2.Text = objEmpresa.codac2;
            txtCodac3.Text = objEmpresa.codac3;
            txtCodac4.Text = objEmpresa.codac4;





        }
        public void f_llenarCamposEmpresaBase(cls_seg_Empresa objEmpresa)
        {
            txtNomdbm.Text = objEmpresa.nomdbm;
            txtNombas.Text = objEmpresa.nombas;
            txtUsubas.Text = objEmpresa.usubas;
            txtClabas.Text = objEmpresa.clabas;
            txtIpserv.Text = objEmpresa.ipserv;
        }
        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }
        public void f_limpiarCampos()
        {

            txtRucemp.Text = txtNomdbm.Text = txtNombas.Text = txtClabas.Text = txtUsubas.Text = txtRucemp.Text = txtIpserv.Text =
            txtNomemp.Text = txtFeccad.Value = txtEspdis.Text = txtNumlic.Text = txtSerlic.Text =
            txtCodac1.Text = txtCodac2.Text = txtCodac3.Text = txtCodac4.Text = "";
            ddlEstcad.SelectedValue = "1";
            lblExito.Text = "";
            lblError.Text = "";

            Session["gs_Rucemp"] = "";

        }
    }
}