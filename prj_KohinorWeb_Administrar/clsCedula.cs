﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb_Administrar
{
    public class clsCedula
    {
        public string Cedula { get; set; }
        public string NombreCiudadano { get; set; }
        public string CondicionCiudadano { get; set; }
        public string FechaNacimiento { get; set; }
        public string LugarNacimiento { get; set; }
        public string Nacionalidad { get; set; }
        public string EstadoCivil { get; set; }
        public string IndividualDactilar { get; set; }
        public string Conyuge { get; set; }
        public string NombrePadre { get; set; }
        public string NacionalidadPadre { get; set; }
        public string NombreMadre { get; set; }
        public string NacionalidadMadre { get; set; }
        public string Domicilio { get; set; }
        public string CallesDomicilio { get; set; }
        public string NumeroCasa { get; set; }
        public string FechaDefuncion { get; set; }
        public string FechaCedulacion { get; set; }
        public string Sexo { get; set; }
        public string Instruccion { get; set; }
        public string Profesion { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string DPA_ProvinDomicilio { get; set; }
        public string DPA_CantonDomicilio { get; set; }
        public string DPA_ParroqDomicilio { get; set; }
        public string DPA_ProvinNacimiento { get; set; }
        public string DPA_CantonNacimiento { get; set; }
        public string DPA_ParroqNacimiento { get; set; }
        public string Fotografia { get; set; }
    }
}