﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_Principal : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //f_Seg_Usuario_Modulo();
            //f_Seg_Usuario_Modulo_Config(); //para modulos Configuracion y Seguridades
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Para light/dark mode
                string s_modo = Session["gs_Modo"].ToString();

                if (s_modo == "L")
                {
                    body.Attributes.Add("class", "c-app");
                    btnligth.Attributes.Add("style", "display: none;");
                }
                if (s_modo == "D")
                {
                    body.Attributes.Add("class", "c-app c-dark-theme");
                    btndark.Attributes.Add("style", "display: none;");
                }

                try
                {   
                    lblEmp.Text = "KOHINOR WEB";
                    lblCodus1.Text = Session["gs_UsuLog"].ToString();
                    lblSersec.Text = Session["gs_SerSec"].ToString();
                    lblUsuario.Text = Session["gs_UsuLog"].ToString();
                    lblNomemp.Text = "KOHINOR WEB";
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = "❗ *01. " + ex.Message;
                }
            }
        }

        protected void btndark_Click(object sender, EventArgs e)
        {
            body.Attributes.Add("class", "c-app c-dark-theme");
            btnligth.Attributes.Add("style", "display: inline;");
            btndark.Attributes.Add("style", "display: none;");

            Session["gs_Modo"] = "D";
        }
        protected void btnligth_Click(object sender, EventArgs e)
        {
            body.Attributes.Add("class", "c-app");
            btndark.Attributes.Add("style", "display: inline;");
            btnligth.Attributes.Add("style", "display: none;");

            Session["gs_Modo"] = "L";
        }
    }
}

