﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb_Administrar
{
    public class clsRootObjectRUC
    {
        public bool success { get; set; }
        public string mensaje { get; set; }
        public List<clsRUC> result { get; set; }
    }
}