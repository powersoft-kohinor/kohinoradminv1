﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb_Administrar
{
    public class cls_seg_Usuario
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        public string Rucemp { get; set; } 
        public string Codus1 { get; set; } 
        public string Nomusu { get; set; }         
        public string Clausu { get; set; }
        public string Codsuc { get; set; }
        public string Sersec { get; set; }
        public string Usmail { get; set; }
        public DateTime Fecexp { get; set; }
        public string Estado { get; set; }
        public string Tipacc { get; set; }
        public string Estusu { get; set; }
        public string Pcnom { get; set; }
        public string Pcmac { get; set; }
        public string Pcip { get; set; }
        public string Nomdbm { get; set; }
        public string Nombas { get; set; }
        public string Usubas { get; set; }
        public string Clabas { get; set; }
        public string Ipserv { get; set; }
        public DateTime? Ultent { get; set; } //? es para que pueda ser null como en la base y no de error
        public DateTime? Ultsal { get; set; }


        public DataTable dtUsuario { get; set; }

        public clsError Error { get; set; }


        public cls_seg_Usuario f_Usuario_Buscar(string s_rucemp, string gs_codcliSelected) //busca 1 solo cliente
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@RUCEMP", s_rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", gs_codcliSelected);

            try
            {
                //using (conn)
                //{
                //SqlDataReader reader = cmd.ExecuteReader();
                //DataTable dt = new DataTable();
                //dt.Load(reader);
                //while (reader.Read())
                //{
                //    string p = reader[1].ToString();
                //    byte[] myImage = (byte[])reader["clausu"];
                //}
                //}



                //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objUsuario = f_dtUsuarioToObjUsuario(dt);
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }
        public cls_seg_Usuario f_Usuario_Base_Buscar(string ip) //para usuario inicial
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_INICIAL_ADMIN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@IP", ip);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1");
                objUsuario.Rucemp = dt.Rows[0].Field<string>("rucemp");
                objUsuario.Nomusu = dt.Rows[0].Field<string>("nomusu");
                objUsuario.Codsuc = dt.Rows[0].Field<string>("codsuc");
                objUsuario.Pcip = dt.Rows[0].Field<string>("pcip");
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
                if (objUsuario.Error.Mensaje.Equals("❗ There is no row at position 0."))
                {
                    objUsuario.Error.Mensaje = "❗ Máquina no registrada.";
                }
            }

            conn.Close();
            return objUsuario;
        }

        public cls_seg_Usuario f_Usuario_EmpresaBase(string s_codus1, string s_pcip, string s_palcla)
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Codus1", s_codus1);
            cmd.Parameters.AddWithValue("@PcIp", s_pcip);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                //DataSet ds = new DataSet();
                //sda.Fill(ds);
                //objUsuario = f_dtUsuarioToObjUsuario(ds.Tables[0]);
                objUsuario.Nomdbm = dt.Rows[0].Field<string>("nomdbm").Trim();
                objUsuario.Nombas = dt.Rows[0].Field<string>("nombas").Trim();
                objUsuario.Usubas = dt.Rows[0].Field<string>("usubas").Trim();
                objUsuario.Clabas = dt.Rows[0].Field<string>("clabas").Trim();
                objUsuario.Ipserv = dt.Rows[0].Field<string>("ipserv").Trim();
            }
            catch (Exception ex)
            {
                objUsuario.Error = objUsuario.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objUsuario;
        }

        public clsError f_Usuario_Actualizar(cls_seg_Usuario objUsuario, string s_usuing, string s_codusu, string s_Action)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_Action);
            cmd.Parameters.AddWithValue("@RUCEMP", objUsuario.Rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@NOMUSU", objUsuario.Nomusu);
            cmd.Parameters.AddWithValue("@USMAIL", objUsuario.Usmail);
            cmd.Parameters.AddWithValue("@FECEXP", objUsuario.Fecexp);
            cmd.Parameters.AddWithValue("@SERSEC", objUsuario.Sersec);
            cmd.Parameters.AddWithValue("@CODSUC", objUsuario.Codsuc);
            cmd.Parameters.AddWithValue("@ESTADO", objUsuario.Estado);
            cmd.Parameters.AddWithValue("@TIPACC", objUsuario.Tipacc);
            cmd.Parameters.AddWithValue("@PCNOM", objUsuario.Pcnom);
            cmd.Parameters.AddWithValue("@PCIP", objUsuario.Pcip);
            cmd.Parameters.AddWithValue("@PCMAC", objUsuario.Pcmac);            
            cmd.Parameters.AddWithValue("@USUING", s_usuing);
            cmd.Parameters.AddWithValue("@CODUSU", s_codusu);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Usuario_Clave(cls_seg_Usuario objUsuarioRegistrar)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_CLAVE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CodUs1", objUsuarioRegistrar.Codus1);
            cmd.Parameters.AddWithValue("@ClaUsu", objUsuarioRegistrar.Clausu);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Usuario_Encriptar_Base(cls_seg_Usuario objUsuario, string s_palcla)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@RucEmp", objUsuario.Rucemp);
            cmd.Parameters.AddWithValue("@CodUs1", objUsuario.Codus1);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);
            cmd.Parameters.AddWithValue("@NomDbm", objUsuario.Nomdbm);
            cmd.Parameters.AddWithValue("@NomBas", objUsuario.Nombas);
            cmd.Parameters.AddWithValue("@UsuBas", objUsuario.Usubas);
            cmd.Parameters.AddWithValue("@ClaBas", objUsuario.Clabas);
            cmd.Parameters.AddWithValue("@IpServ", objUsuario.Ipserv);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public clsError f_Usuario_Eliminar(string s_rucemp, string s_codus1)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@RUCEMP", s_rucemp);
            cmd.Parameters.AddWithValue("@CODUS1", s_codus1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }

        public cls_seg_Usuario f_dtUsuarioToObjUsuario(DataTable dt) //para pasar de dt a objUsuario los atributos de la base
        {
            cls_seg_Usuario objUsuario = new cls_seg_Usuario();
            objUsuario.Error = new clsError();
            objUsuario.dtUsuario = dt;
            objUsuario.Rucemp = dt.Rows[0].Field<string>("rucemp");
            objUsuario.Codus1 = dt.Rows[0].Field<string>("codus1");
            objUsuario.Nomusu = dt.Rows[0].Field<string>("nomusu");
            //objUsuario.Clausu = System.Text.Encoding.ASCII.GetString(dt.Rows[0].Field<byte[]>("clausu"));
            objUsuario.Codsuc = dt.Rows[0].Field<string>("codsuc");
            objUsuario.Sersec = dt.Rows[0].Field<string>("sersec");
            objUsuario.Usmail = dt.Rows[0].Field<string>("usmail");
            objUsuario.Fecexp = dt.Rows[0].Field<DateTime>("fecexp");
            objUsuario.Estado = dt.Rows[0].Field<string>("estado");
            objUsuario.Tipacc = dt.Rows[0].Field<string>("tipacc");            
            objUsuario.Estusu = dt.Rows[0].Field<string>("estusu");
            objUsuario.Ultent = dt.Rows[0].Field<DateTime?>("ultent");
            objUsuario.Ultsal = dt.Rows[0].Field<DateTime?>("ultsal");
            objUsuario.Pcnom = dt.Rows[0].Field<string>("pcnom");
            objUsuario.Pcip = dt.Rows[0].Field<string>("pcip");
            objUsuario.Pcmac = dt.Rows[0].Field<string>("pcmac");

            return objUsuario;
        }

        public clsError f_Usuario_Auditoria(string s_rucemp, string s_codus1, string s_usuing, DateTime? s_fecing, string s_codusu, DateTime? s_fecult)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE [seg_usuario] SET [usuing] = @Usuing, [fecing] = @Fecing, [codusu] = @Codusu, [fecult] = @Fecult WHERE [rucemp] = @Rucemp AND [codus1] = @Codus1 ", conn);
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);
            cmd.Parameters.AddWithValue("@Rucemp", s_rucemp);
            cmd.Parameters.AddWithValue("@Codus1", s_codus1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}