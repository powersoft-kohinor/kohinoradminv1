﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualBasic;



namespace prj_KohinorWeb_Administrar
{
    public class cls_seg_Encriptacion
    {
        public string f_seg_Encriptar(string s_Clabas)
        {
            String s_Claenc, s_aux;
            int i, j, r_Clave;

            j = s_Clabas.Length;
            s_Claenc = "";
            r_Clave = j % 3;

            if (r_Clave == 0)
            {
                r_Clave = 3;
            }
            else
            {
                r_Clave = j % 2;
                if (r_Clave == 0)
                {
                    r_Clave = 2;
                }
                else
                {
                    r_Clave = 1;
                }
            }


            for (i = 1; i <= j; i++)
            {
                s_aux = Strings.Mid(s_Clabas, i, 1);
                s_aux = Convert.ToString(Convert.ToChar(Strings.Asc(s_aux) - 30 + i + r_Clave));
                s_Claenc = s_Claenc + s_aux;
            }


            return s_Claenc;
        }
        public string f_seg_Encriptar_Rev(string s_Clabas)
        {


            String s_Claenc, s_aux;
            int i, j, r_Clave;

            j = s_Clabas.Length;
            s_Claenc = "";
            r_Clave = j % 3;

            if (r_Clave == 0)
            {
                r_Clave = 3;
            }
            else
            {
                r_Clave = j % 2;
                if (r_Clave == 0)
                {
                    r_Clave = 2;
                }
                else
                {
                    r_Clave = 1;
                }
            }


            for (i = 1; i <= j; i++)
            {
                s_aux = Strings.Mid(s_Clabas, i, 1);
                s_aux = Convert.ToString(Convert.ToChar(Strings.Asc(s_aux) + 30 - i - r_Clave));
                s_Claenc = s_Claenc + s_aux;
            }


            return s_Claenc;
        }
    }
}