﻿using prj_KohinorWeb_Administrar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb
{
    public partial class w_Login : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);
        protected cls_seg_Usuario objUsuario = new cls_seg_Usuario();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) //para llenar ddlEmpresa
            {
                Session.Clear();
                //imgLogo.ImageUrl = dt.Rows[0].Field<string>("logsis").Trim();

                lblError.Visible = false;

                Session.Add("gs_CodEmp", "01");
                Session.Add("gs_Error", "");
                Session.Add("gd_FecAct", DateTime.Now.ToString("yyy-MM-dd"));
                Session.Add("gs_Modo", "L"); //Para light/dark mode
                Session.Add("gs_Ip", f_GetIp());
                objUsuario = objUsuario.f_Usuario_Base_Buscar(Session["gs_Ip"].ToString());
                if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
                    txtUsuario.Value = objUsuario.Codus1;
                else
                {
                    lblError.Text = objUsuario.Error.Mensaje;
                    lblError.Visible = true;
                }
                //Aqui se asigna el nombre de cada uno de los elementos segun el idima base de datos 
                lblIncioSesion.Text = "Iniciar Sesión";
                lblIngresacuenta.Text = "Ingresa a tu cuenta";
                txtUsuario.Attributes.Add("placeholder", "Usuario");
                txtPassword.Attributes.Add("placeholder", "Contraseña");
                btnLogin.Text = "Login";
                btnRecuperar.Text = "Olvidaste tu contraseña?";
            }
        }

        public string f_GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ip = "HTTP_X_FORWARDED_FOR IS NULL";
            }
            return ip;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //OJO BORRAR
            //txtUsuario.Value = "WW";
            //txtPassword.Value = "123";
            lblError.Text = "";
            DataTable dt = new DataTable();
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_USUARIO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Codus1", txtUsuario.Value);
            cmd.Parameters.AddWithValue("@ClaUsu", txtPassword.Value);
            cmd.Parameters.AddWithValue("@IP", Session["gs_Ip"].ToString());

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["existe"].ToString().Trim().Equals("1"))
                        {
                            objUsuario.Codus1 = dt.Rows[0]["codus1"].ToString().Trim();
                            objUsuario.Pcip = dt.Rows[0]["pcip"].ToString().Trim();
                            Session.Add("gs_CodUs1", dt.Rows[0]["codus1"].ToString().Trim());
                            Session.Add("gs_UsuLog", dt.Rows[0]["codus1"].ToString().Trim());
                            Session.Add("gs_Sersec", dt.Rows[0]["sersec"].ToString().Trim());

                        }
                        else if (dt.Rows[0]["existe"].ToString().Trim().Equals("0")) 
                        {
                            lblError.Text = "❗ Máquina no registrada.";
                            lblError.Visible = true;
                        }
                        else if (dt.Rows[0]["existe"].ToString().Trim().Equals("2"))
                        {
                            lblError.Text = "*Credenciales no concuerdan.";
                            lblError.Visible = true;
                        }
                    }                    
                }
                else
                {
                    lblError.Text = "*Credenciales no concuerdan.";
                    lblError.Visible = true;
                }
            }
            catch (Exception ex) //ERROR EN SP
            {
                lblError.Visible = true;
                lblError.Text = "❗ *01. " + ex.Message;
            }
            conn.Close();

            Response.Redirect("w_seg_AdministracionEmpresa.aspx");

            //if (String.IsNullOrEmpty(lblError.Text))
            //{
            //    if (!Session["gs_Ip"].ToString().Equals(objUsuario.Pcip))
            //    {
            //        lblError.Text = "*No tiene acceso desde esta localización.";
            //        lblError.Visible = true;
            //    }
            //    else
            //    {
            //        Response.Redirect("w_seg_AdministracionEmpresa.aspx");
            //    }
            //}
        }
    }
}