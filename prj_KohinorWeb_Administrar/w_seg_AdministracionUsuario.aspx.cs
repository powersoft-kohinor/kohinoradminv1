﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace prj_KohinorWeb_Administrar
{
    public partial class w_seg_AdministracionUsuario : System.Web.UI.Page
    {
        protected cls_seg_Usuario objUsuario = new cls_seg_Usuario();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_UsuLog"] == null) Response.Redirect("w_Login.aspx"); // Check if the user is not logged in
                f_BindGrid_Inicial(); //grvUsuario
                //OBTENER PALCLA
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsPalabraClave.Select(args);
                DataTable dt = view.ToTable();

                Session.Add("gs_PalCla", dt.Rows[0]["palcla"]);
                Session.Add("gs_Rucemp", ""); //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
                Session.Add("gs_Codus1", "");
            }        
        }

        protected void f_ErrorNuevo(clsError objError)
        {
            lblModalFecha.Text = objError.Fecha.ToString();
            txtModalNum.Text = objError.NoError;
            lblModalError.Text = objError.Mensaje;
            lblModalPagina.Text = objError.Donde;
            txtModalObjeto.Text = objError.Objeto;
            txtModalEvento.Text = objError.Evento;
            txtModalLinea.Text = objError.Linea;
        }

        protected void btnNuevo_Click(object sender, EventArgs e) => f_limpiarCampos();
        protected void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            lblExito.Text = "";
            lblError.Text = "";
            if (String.IsNullOrEmpty(txtRucemp.Text) || String.IsNullOrEmpty(txtCodus1.Value) || String.IsNullOrEmpty(txtNomusu.Value) || String.IsNullOrEmpty(txtEmail.Value) ||
                    String.IsNullOrEmpty(txtPcnom.Text) || String.IsNullOrEmpty(txtPcmac.Text) || String.IsNullOrEmpty(txtPcip.Text) || String.IsNullOrEmpty(txtFecexp.Text))
            {
                lblError.Text = "Debe llenar todos los campos para guardar. ";
            }
            else
            {
                if (chkClausu.Checked)
                {
                    if (String.IsNullOrEmpty(txtClausu.Value))
                    {
                        lblError.Text = "Debe llenar la clave del usuario.";
                    }
                }

                if (String.IsNullOrEmpty(lblError.Text))
                {
                    clsError objError = new clsError();
                    cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
                    string s_PalCla = objEncriptacion.f_seg_Encriptar(Session["gs_PalCla"].ToString());

                    objUsuario.Rucemp = txtRucemp.Text.Trim();
                    objUsuario.Codus1 = txtCodus1.Value.Trim();
                    objUsuario.Nomusu = txtNomusu.Value.Trim();
                    objUsuario.Clausu = txtClausu.Value.Trim();
                    objUsuario.Usmail = txtEmail.Value.Trim();
                    objUsuario.Pcnom = txtPcnom.Text.Trim();
                    objUsuario.Pcmac = txtPcmac.Text.Trim();
                    objUsuario.Pcip = txtPcip.Text.Trim();
                    objUsuario.Estusu = txtEstusu.Text.Trim();

                    objUsuario.Nomdbm = String.IsNullOrEmpty(txtNomdbm.Text.Trim()) ? null : txtNomdbm.Text.Trim();
                    objUsuario.Nombas = String.IsNullOrEmpty(txtNombas.Text.Trim()) ? null : txtNombas.Text.Trim();
                    objUsuario.Usubas = String.IsNullOrEmpty(txtUsubas.Text.Trim()) ? null : txtUsubas.Text.Trim();
                    objUsuario.Clabas = String.IsNullOrEmpty(txtClabas.Text.Trim()) ? null : txtClabas.Text.Trim();
                    objUsuario.Ipserv = String.IsNullOrEmpty(txtIpserv.Text.Trim()) ? null : txtIpserv.Text.Trim();
                    try
                    {
                        objUsuario.Fecexp = DateTime.Parse(txtFecexp.Text);
                        //s_fecnac = d_date.ToString("yyyy-MM-dd");
                    }
                    catch (Exception ex) //si tiene fecnac en NULL
                    {
                        lblError.Text = "Error al convertir fecexp.";
                        //objUsuario.Fecexp = DateTime.Now.Date;
                    }
                    objUsuario.Estado = ddlEstado.SelectedValue.ToString();
                    objUsuario.Tipacc = ddlTipacc.SelectedValue.ToString();
                    objUsuario.Sersec = String.IsNullOrEmpty(txtSerie.Text) ? null : txtSerie.Text.Trim();
                    objUsuario.Codsuc = String.IsNullOrEmpty(txtSucursal.Text) ? null : txtSucursal.Text.Trim();

                    DataSourceSelectArguments args = new DataSourceSelectArguments();
                    DataView view = (DataView)sqldsUsuarioExiste.Select(args);
                    if (view != null) //ya existe el usuario (Actualizar)
                    {
                        objError = objUsuario.f_Usuario_Actualizar(objUsuario, "ADMIN", "ADMIN", "Update");
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            if (chkClausu.Checked)
                            {
                                objError = objUsuario.f_Usuario_Clave(objUsuario); //guarda pass encriptado
                                if (String.IsNullOrEmpty(objError.Mensaje))
                                {
                                    Session["gs_Rucemp"] = objUsuario.Rucemp;
                                    Session["gs_Codus1"] = objUsuario.Codus1;
                                    lblError.Text = "";
                                    lblExito.Text = "✔️ Usuario Modificado.";
                                    f_BindGrid_Inicial();
                                }
                                else
                                {
                                    f_ErrorNuevo(objError);
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                }
                            }
                            else
                            {
                                Session["gs_Rucemp"] = objUsuario.Rucemp;
                                Session["gs_Codus1"] = objUsuario.Codus1;
                                lblError.Text = "";
                                lblExito.Text = "✔️ Usuario Modificado.";
                                f_BindGrid_Inicial();
                            }
                            
                        }
                        else
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                    else //no existe el usuario (Insertar)
                    {
                        objError = objUsuario.f_Usuario_Actualizar(objUsuario, "ADMIN", "ADMIN", "Add");

                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            if (chkClausu.Checked)
                            {
                                objError = objUsuario.f_Usuario_Clave(objUsuario); //guarda pass encriptado
                                if (String.IsNullOrEmpty(objError.Mensaje))
                                {
                                    Session["gs_Rucemp"] = objUsuario.Rucemp;
                                    Session["gs_Codus1"] = objUsuario.Codus1;
                                    lblError.Text = "";
                                    lblExito.Text = "✔️ Usuario Creado.";
                                    f_BindGrid_Inicial();
                                }
                                else
                                {
                                    f_ErrorNuevo(objError);
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                                }
                            }
                            else
                            {
                                Session["gs_Rucemp"] = objUsuario.Rucemp;
                                Session["gs_Codus1"] = objUsuario.Codus1;
                                lblError.Text = "";
                                lblExito.Text = "✔️ Usuario Creado.";
                                f_BindGrid_Inicial();
                            }                            
                        }
                        else
                        {
                            f_ErrorNuevo(objError);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }

                    //VALIDA AUDITORIA
                    //objUsuario.f_Usuario_Auditoria(objUsuario.Rucemp, objUsuario.Codus1, Session["gs_UsuIng"].ToString(), DateTime.Now, Session["gs_CodUs1"].ToString(), DateTime.Now);
                    objUsuario.f_Usuario_Auditoria(objUsuario.Rucemp, objUsuario.Codus1, "ADMIN", DateTime.Now, "ADMIN", DateTime.Now);
                    if (String.IsNullOrEmpty(objError.Mensaje))
                    {
                        objError = objUsuario.f_Usuario_Encriptar_Base(objUsuario, s_PalCla);
                        if (String.IsNullOrEmpty(objError.Mensaje))
                        {
                            lblExito.Text = lblExito.Text + " - Datos de Conexión Registrados Correctamente.";
                        }
                        else
                        {
                            lblExito.Text = lblExito.Text + " - Datos de Conexión No Registrados.";
                            //f_ErrorNuevo(objError);
                            //ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
                        }
                    }
                }                
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e) => f_limpiarCampos();
        protected void lkbtnDelete_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Session["gs_Rucemp"].ToString()))
            {
                Session["gs_Rucemp"] = txtRucemp.Text;
                lblEliUsu.Text = Session["gs_Rucemp"].ToString() + " - " + Session["gs_Codus1"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_Eliminar();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
            else
            {
                lblError.Text = "❗ *02. " + DateTime.Now + " Debe seleccionar un cliente para eliminar.";
            }
        }
        protected void btnEliminarFactura_Click(object sender, EventArgs e)
        {
            clsError objError = objUsuario.f_Usuario_Eliminar(Session["gs_Rucemp"].ToString(), Session["gs_Codus1"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
            {
                f_limpiarCampos();
                f_BindGrid_Inicial(); //grvUsuario
                lblExito.Text = "✔️ Usuario Eliminado.";
            }
            else
            {
                f_ErrorNuevo(objError);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }

        //***i GRVUSUARIO ***//
        protected void grvUsuario_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;

            }
        }
        protected void grvUsuario_RowCreated(object sender, GridViewRowEventArgs e) //fires on every postback regardless of whether the data has changed
        {

            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        {
                            if (lnk != null && lnk.CommandArgument != null)
                            {
                                // inizialize a new image
                                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                                // setting the dynamically URL of the image
                                lbl.Text = " ↑↓";
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(lbl);
                            }
                        }
                    }
                }
            }



        }
        protected void grvUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            f_limpiarCampos();
            GridViewRow fila = grvUsuario.SelectedRow;
            string s_rucemp = fila.Cells[2].Text.Trim();
            string s_codus1 = fila.Cells[3].Text.Trim();
            string s_pcip = fila.Cells[8].Text.Trim();

            Session["gs_Rucemp"] = s_rucemp; //variable para que al seleccionar uno en el grv se obtenga el codcli (sg_codcliSELECTED ->Editar)
            Session["gs_Codus1"] = s_codus1;
            cls_seg_Encriptacion objEncriptacion = new cls_seg_Encriptacion();
            string s_PalCla = objEncriptacion.f_seg_Encriptar(Session["gs_PalCla"].ToString());
            objUsuario = objUsuario.f_Usuario_EmpresaBase(s_codus1, s_pcip, s_PalCla);
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                f_llenarCamposEmpresaBase(objUsuario);
            }
            else
            {
                lblError.Text = "No hay Datos de Conexción Registrados.";
                //f_ErrorNuevo(objUsuario.Error);
                //ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }

            objUsuario = objUsuario.f_Usuario_Buscar(s_rucemp, s_codus1);
            if (String.IsNullOrEmpty(objUsuario.Error.Mensaje))
            {
                f_llenarCamposUsuario(objUsuario);
            }
            else
            {
                f_ErrorNuevo(objUsuario.Error);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalError();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
            }
        }
        private void f_BindGrid_Inicial()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsUsuario.Select(args); //procedimiento almacenado
            DataTable dt = view.ToTable();
            Session.Add("gdt_Usuario", dt);

            grvUsuario.DataSource = dt;
            int numRecord = dt.AsEnumerable().Count(x => x.Field<String>("estado") == "0");
            Label txtNotificacionEstusu= this.Master.FindControl("txtNotificacionUsuarios") as Label;
            if (txtNotificacionEstusu != null)
                txtNotificacionEstusu.Text = numRecord.ToString();

            
            f_BindGrid();
        }
        private void f_BindGrid(string sortExpression = null, string searchText = null)
        {
            DataTable dt = (DataTable)Session["gdt_Usuario"];
            using (dt)
            {
                DataView dv = new DataView(dt);
                if (searchText != null)
                {
                    if (searchText.Equals(""))
                        searchText = "%";
                    dv.RowFilter = "rucemp LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "codus1 LIKE " + "'%" + searchText.Trim() + "%'" + " OR " + "nomusu LIKE " + "'%" + searchText.Trim() + "%'";
                    grvUsuario.DataSource = dv;
                }

                if (sortExpression != null)
                {
                    //DataView dv = dt.AsDataView();
                    this.SortDirection = this.SortDirection == "ASC" ? "DESC" : "ASC";

                    dv.Sort = sortExpression + " " + this.SortDirection;
                    grvUsuario.DataSource = dv;
                }
                grvUsuario.DataBind();
                foreach (GridViewRow row in grvUsuario.Rows)
                {
                    if (row.Cells[9].Text.Trim().Equals("0")) //pinta la fila con codart
                    {
                        row.BackColor = Color.LightCoral;
                        row.ForeColor = Color.Black;
                    }                   

                    if (row.Cells[9].Text.Trim().Equals("1")) //pinta la fila con codart
                    {
                        row.BackColor = Color.PaleGreen;
                    }

                    if (row.Cells[9].Text.Trim().Equals("2")) //pinta la fila con codart
                    {
                        row.BackColor = Color.LightGoldenrodYellow;
                    }

                }
            }
        }
        private string SortDirection
        {
            get { return ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected void f_GridBuscar(object sender, EventArgs e)
        {
            f_BindGrid(null, txtSearchUsuario.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "f_ModalUsuario();", true); //OJO REQUIERE jquery-3.4.1.min.js PARA FUNCIONAR
        }
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvUsuario.PageIndex = e.NewPageIndex;
            f_BindGrid(null, txtSearchUsuario.Text);
        }
        protected void OnSorting(object sender, GridViewSortEventArgs e)
        {
            f_BindGrid(e.SortExpression, txtSearchUsuario.Text);
        }
        public void f_llenarCamposUsuario(cls_seg_Usuario objUsuario)
        {
            txtRucemp.Text = objUsuario.Rucemp;
            txtCodus1.Value = objUsuario.Codus1;
            txtNomusu.Value = objUsuario.Nomusu;
            txtEmail.Value = objUsuario.Usmail;

            txtPcnom.Text = objUsuario.Pcnom;
            txtPcmac.Text = objUsuario.Pcmac;
            txtPcip.Text = objUsuario.Pcip;
            txtEstusu.Text = objUsuario.Estusu;
            txtEntrada.Text = objUsuario.Ultent.ToString();
            txtSalida.Text = objUsuario.Ultsal.ToString();

            ddlEstado.SelectedValue = objUsuario.Estado;
            ddlTipacc.SelectedValue = objUsuario.Tipacc;

            txtFecexp.Text = objUsuario.Fecexp.ToString("yyyy-MM-dd");
            txtSucursal.Text = objUsuario.Codsuc;
            txtSucursal.Text = objUsuario.Codsuc;
            txtSerie.Text = objUsuario.Sersec;
        }
        public void f_llenarCamposEmpresaBase(cls_seg_Usuario objUsuario)
        {
            txtNomdbm.Text = objUsuario.Nomdbm;
            txtNombas.Text = objUsuario.Nombas;
            txtUsubas.Text = objUsuario.Usubas;
            txtClabas.Text = objUsuario.Clabas;
            txtIpserv.Text = objUsuario.Ipserv;
        }

        //***f GRVUSUARIO ***//

        public void f_limpiarCampos()
        {
            txtRucemp.Text = txtCodus1.Value = txtNomusu.Value = txtEmail.Value = txtPcnom.Text = txtPcmac.Text = txtPcip.Text = txtEstusu.Text = txtEntrada.Text =
                txtSalida.Text = txtNomdbm.Text = txtNombas.Text = txtUsubas.Text = txtClabas.Text = txtIpserv.Text = txtFecexp.Text = txtSerie.Text = txtSucursal.Text = "";

            lblExito.Text = "";
            lblError.Text = "";
            Session["gs_Rucemp"] = "";
            Session["gs_Codus1"] = "";
        }

        //public bool f_validarCampos()
        //{
        //    if (String.IsNullOrEmpty(txtRucemp.Text) || String.IsNullOrEmpty(txtCodus1.Value) || String.IsNullOrEmpty(txtNomusu.Value) || String.IsNullOrEmpty(txtEmail.Value) ||
        //            String.IsNullOrEmpty(txtPcnom.Text) || String.IsNullOrEmpty(txtPcmac.Text) || String.IsNullOrEmpty(txtPcip.Text) || String.IsNullOrEmpty(txtFecexp.Text))
        //    {
        //        lblError.Text = "Debe llenar todos los campos para guardar. ";
        //    }
        //    return true;
        //}
    }
}