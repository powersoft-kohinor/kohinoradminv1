﻿<%@ Page Title="Empresa" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_seg_AdministracionEmpresa.aspx.cs" Inherits="prj_KohinorWeb_Administrar.w_seg_AdministracionEmpresa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Kohinor Admin - Empresa</title>
	  <script type="text/javascript">
		  function f_ModalEmpresa() {
			  $("#btnShowEmpresaModal").click();
		  }
	</script>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
	<div class="c-subheader justify-content-between px-3">
		<!-- Breadcrumb-->
		<ol class="breadcrumb border-0 m-0">
			<li class="breadcrumb-item active"><h5>Administración Empresa</h5></li>
			<!-- Breadcrumb Menu-->
		</ol>
		<div class="c-subheader-nav d-md-down-none mfe-2">
            <div id="spinner" class="spinner-border" role="status">
				<span class="sr-only">Loading...</span>
			</div>
			<%--<a class="c-subheader-nav-link">
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn btn-primary" type="button" id="">
							<i class="cil-filter"></i>
						</button>
					</div>

					<asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
				</div>
				<asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
			</a>	--%>		
		</div>
	</div>
	<div class="c-subheader px-3">
		<ol class="breadcrumb border-0 m-0">
			<li class="breadcrumb-item active">
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click"><i class="cil-file"></i>Nuevo</button>
					<button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" data-toggle="modal" data-target="#EmpresaModal"><i class="cil-folder-open"></i>Abrir</button>
					<button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardar_Click"><i class="cil-save"></i>Guardar</button>
					<button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click"><i class="cil-grid-slash"></i>Cancelar</button>
					<button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click"><i class="cil-trash"></i>Eliminar</button>
				</div>
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-secondary disabled" runat="server" onserverclick="lkbtnPrincipio_Click"><i class="cil-media-step-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled" runat="server" onserverclick="lkbtnAtras_Click"><i class="cil-media-skip-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled" runat="server" onserverclick="lkbtnSiguiente_Click"><i class="cil-media-skip-forward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled" runat="server" onserverclick="lkbtnFinal_Click"><i class="cil-media-step-forward"></i></button>
				</div>

				<asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
				<asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
			</li>
		</ol>

	</div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="fade-in">
                    <div class="card bg-gradient-secondary">
                        <div class="card-header">
                            <i class="c-icon cil-building" style="padding-right:30px;"></i><b>Administración de Empresa</b>
                        </div>

                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <h5><strong>Identificación</strong></h5>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Ruc/C.I.</strong></span>
                                                    </div>
                                                    <asp:TextBox type="text" ID="txtRucemp" class="form-control" placeholder="RUC Empresa" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Nombre</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtNomemp" runat="server" class="form-control" placeholder="Nombre Empresa"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->

                                    <h5><strong>Datos de Conexión</strong></h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Dbms</strong></span>
                                                    </div>
                                                    <asp:TextBox type="text" ID="txtNomdbm" class="form-control" runat="server" placeholder="Dbms"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Base</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtNombas" runat="server" class="form-control" placeholder="Nombas"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Usuario</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtUsubas" runat="server" class="form-control" placeholder="Usubas"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Clave</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtClabas" runat="server" class="form-control" placeholder="Clabas"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>IP Servidor</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtIpserv" runat="server" class="form-control" placeholder="IPServ"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h5><strong>Datos de Licencia</strong></h5>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Licencia Adquirida</strong></span>
                                                    </div>
                                                    <asp:DropDownList ID="ddlEstcad" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlEstcad_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Perpetua</asp:ListItem>
                                                    <asp:ListItem Value="2">Parcial</asp:ListItem>
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <input type="date" id="txtFeccad" class="form-control" runat="server" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Tipo de Licencia</strong></span>
                                                    </div>
                                                    <asp:TextBox type="text" ID="txtNumlic" class="form-control" runat="server" TextMode="Number" placeholder="Numlic"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Espacio Contratado</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtEspdis" runat="server" class="form-control" TextMode="Number" placeholder="Espdis"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>N° Serie</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtSerlic" runat="server" class="form-control" placeholder="Serlic"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Clave</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtCodac1" runat="server" class="form-control" placeholder="txtCodac1"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodac2" runat="server" class="form-control" placeholder="txtCodac2"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodac3" runat="server" class="form-control" placeholder="txtCodac3"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodac4" runat="server" class="form-control" placeholder="txtCodac4"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsPalabraClave" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [palcla] FROM [seg_configuracion]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEmpresa" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [rucemp], [nomemp], [estcad], [feccad], [numlic], [espdis] FROM [seg_empresa]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsEmpresaExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [rucemp] FROM [seg_empresa] WHERE ([rucemp] = @rucemp)">
        <SelectParameters>
            <asp:SessionParameter Name="rucemp" SessionField="gs_Rucemp" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--f SQL DATASOURCES-->
    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Empresa</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar la Empresa '<asp:Label ID="lblEliEmp" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarEmpresa_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal grvUsuario -->
    <button id="btnShowEmpresaModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#EmpresaModal"></button>
    <div class="modal fade" id="EmpresaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Empresa</h4>
                    <button class="close" id="btnCloseEmpresaModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-row">

                        <div class="form-group col-md-12">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnSearchEmpresa" runat="server" OnClick="f_GridBuscar" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchEmpresa" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-md-12">

                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updEmpresa" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <asp:GridView ID="grvEmpresa" class="table table-bordered table-active table-active table-hover table-striped"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="grvEmpresa_RowDataBound" OnRowCreated="grvEmpresa_RowCreated" OnSelectedIndexChanged="grvEmpresa_SelectedIndexChanged"
                                                AllowSorting="true" OnSorting="OnSorting" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updEmpresaSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectEmpresa" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectEmpresa" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="rucemp" HeaderText="RUC" SortExpression="rucemp" />
                                                    <asp:BoundField DataField="nomemp" HeaderText="Nombre" SortExpression="nomemp" />
                                                    <asp:BoundField DataField="estcad" HeaderText="Estado" SortExpression="estcad" />
                                                    <asp:BoundField DataField="feccad" HeaderText="Fecha" SortExpression="feccad" />
                                                    <asp:BoundField DataField="numlic" HeaderText="Licencia" SortExpression="numlic" />
                                                    <asp:BoundField DataField="espdis" HeaderText="Espacio" SortExpression="espdis" />

                                                </Columns>
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%=txtRucemp.ClientID%>").blur(function () {
                     getValues();
                 });
             });
        </script>
        <script type="text/javascript">
            //validar y obtener cedula de web api sri
            function getValues() {
                var text1 = $("#<%=txtRucemp.ClientID%>").val();
                PageMethods.f_Validar(text1, getValues_Success, getValues_Fail);
            }
            function getValues_Success(data) {
                if (data.TipInd == "-" || data.TipEmp == "-") {
                    //alert("Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos");
                    alert(data.Error);

                }
                else {

                    $("#<%=txtNomemp.ClientID%>").val(data.Nompro);
                }
            }
            function getValues_Fail() {
                alert("ERROR : Método getValues_Success(data) fallo.");
            }
        </script>



    </div>
</asp:Content>

