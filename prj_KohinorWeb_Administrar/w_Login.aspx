﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_Login.aspx.cs" Inherits="prj_KohinorWeb.w_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kohinor Admin - Login</title>
     <%-- Estilo de la pagina Core UI --%>
    <link href="CoreUI/css/style.css" rel="stylesheet" />

    <%-- Estilo del side nav extra --%>
    <link href="CoreUI/css/style2.css" rel="stylesheet" />

    <%-- Core Ui iconos --%>
    <link href="CoreUI/vendors/@coreui/icons/css/brand.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/flag.min.css" rel="stylesheet" />
    <link href="CoreUI/vendors/@coreui/icons/css/free.min.css" rel="stylesheet" />


    <!-- CoreUI and necessary plugins-->
    <script src="CoreUI/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="CoreUI/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="CoreUI/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
    <script src="CoreUI/vendors/@coreui/utils/js/coreui-utils.js"></script>
    <script src="CoreUI/js/main.js"></script>

    <%-- Core Ui script --%>
     <script>
        window.dataLayer = window.dataLayer || [];

        
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>
   
    
</head>
<body>
    <form id="form1" runat="server">
         <div class="c-app flex-row align-items-center fondoLogin">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <div class="text-center">
                    <asp:Image ID="Image1" src="Icon\Menu125\kohinor.png" runat="server" width="75"/>
                </div>
                <hr />
                  
                 
                <h2><asp:Label ID="lblIncioSesion" runat="server" Text=""></asp:Label></h2>
                <p class="text-muted"><asp:Label ID="lblIngresacuenta" runat="server" Text=""></asp:Label></p>
                <div class="input-group mb-3">
                  <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                      </svg></span></div>
                   <input type="text" class="form-control form-control-user" id="txtUsuario" aria-describedby="emailHelp" placeholder="" runat="server"/>
                 
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="CoreUI/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                      </svg></span></div>
                   <input type="password" class="form-control form-control-user" id="txtPassword" placeholder="" runat="server"/>
                </div>
                   <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                <div class="row">
                  <div class="col-6">
                     <asp:Button ID="btnLogin" runat="server" Text="" class="btn btn-primary px-4" OnClick="btnLogin_Click" />
                  </div>
                  <div class="col-6 text-right">
                      <asp:Button class="btn btn-link px-0" ID ="btnRecuperar" type="button" runat="server" Text="" />
                  </div>
                </div>
              </div>
            </div>
            <div class="card text-white bg-primary d-md-down-none" >
             
                <asp:Image ID="imgLogo" style="width: 100%; height: 100%;" runat="server" />
              
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

      <!--i SQL DATASOURCES-->
      <asp:SqlDataSource ID="sqldsSerie" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [sersec] FROM [seg_usuario_serie]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="sqldsPaginaInicial" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT ref002 FROM [seg_modulo_niv002] WHERE (([codmod] = @codmod) AND ([niv001] = @niv001) AND ([niv002] = @niv002))">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="3" Name="codmod" SessionField="gs_Codmod" Type="Int32" />
                <asp:Parameter DefaultValue="2" Name="niv001" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="niv002" Type="Int32" />
            </SelectParameters>
         </asp:SqlDataSource>
      <!--f SQL DATASOURCES-->

    </form>
</body>
</html>
