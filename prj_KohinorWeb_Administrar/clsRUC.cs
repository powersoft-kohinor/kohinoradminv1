﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_KohinorWeb_Administrar
{
    public class clsRUC
    {
        //public List<clsSucursales> sucursales { get; set; }
        public string razonSocial { get; set; }
        public string nroRUC { get; set; }
        public string nombreComercial { get; set; }
        public string estadoContribuyente { get; set; }
        public string claseContribuyente { get; set; }
        public string tipoContribuyente { get; set; }
        public string obligadoContabilidad { get; set; }
        public string actividadEconomica { get; set; }
        public string fechaInicioActividades { get; set; }
        public string fechaCeseActividades { get; set; }
        public string fechaReinicioActividades { get; set; }
        public string fechaActualizacion { get; set; }
        public string categoriaMiPYMES { get; set; }
        public string representanteLegal { get; set; }
        public string agenteRepresentante { get; set; }
        public string direccionContribuyente { get; set; }
        public string DPA_CodigoProvincia { get; set; }
        public string DPA_NombreProvincia { get; set; }
        public string DPA_CodigoCanton { get; set; }
        public string DPA_NombreCanton { get; set; }
        public string DPA_CodigoParroquia { get; set; }
        public string DPA_NombreParroquia { get; set; }

    }
}