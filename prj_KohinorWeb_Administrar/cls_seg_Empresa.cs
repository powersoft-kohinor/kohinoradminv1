﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;

namespace prj_KohinorWeb_Administrar
{
    public class cls_seg_Empresa
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLCA"].ConnectionString);

        public string nomdbm { get; set; }
        public string rucemp { get; set; }
        public string nomemp { get; set; }
        public Decimal espdis { get; set; }
        public string nombas { get; set; }
        public string usubas { get; set; }
        public string clabas { get; set; }
        public string ipserv { get; set; }
        public string estcad { get; set; }
        public DateTime feccad { get; set; }
        public string numlic { get; set; }
        public string serlic { get; set; }
        public string codac1 { get; set; }
        public string codac2 { get; set; }
        public string codac3 { get; set; }
        public string codac4 { get; set; }
        public string Codpro { get; set; }
        public string TipInd { get; set; } //para validar cedula
        public string TipEmp { get; set; } //para validar cedula
        public string RucCed { get; set; } //para validar cedula
        public string Error1 { get; set; } //para validar cedula
        public string Nompro { get; set; }


        public DataTable dtEmpresa { get; set; }
        public clsError Error { get; set; }





        public cls_seg_Empresa f_Empresa_Buscar(string gs_codcliSelected) //busca 1 solo cliente
        {
            cls_seg_Empresa objEmpresa = new cls_seg_Empresa();
            objEmpresa.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_EMPRESA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@RUCEMP", gs_codcliSelected);

            try
            {

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objEmpresa = f_dtEmpresaToObjEmpresa(dt);
            }
            catch (Exception ex)
            {
                objEmpresa.Error = objEmpresa.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objEmpresa;
        }
        public clsError f_Empresa_CRUD(cls_seg_Empresa objEmpresa, string s_usuing, string s_codusu, string s_action)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_EMPRESA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", s_action);
            cmd.Parameters.AddWithValue("@RucEmp", objEmpresa.rucemp);
            cmd.Parameters.AddWithValue("@nomemp", objEmpresa.nomemp);
            cmd.Parameters.AddWithValue("@estcad", objEmpresa.estcad);
            cmd.Parameters.AddWithValue("@feccad", objEmpresa.feccad);
            cmd.Parameters.AddWithValue("@espdis", objEmpresa.espdis);
            cmd.Parameters.AddWithValue("@numlic", objEmpresa.numlic);
            cmd.Parameters.AddWithValue("@serlic", objEmpresa.serlic);
            cmd.Parameters.AddWithValue("@codac1", objEmpresa.codac1);
            cmd.Parameters.AddWithValue("@codac2", objEmpresa.codac2);
            cmd.Parameters.AddWithValue("@codac3", objEmpresa.codac3);
            cmd.Parameters.AddWithValue("@codac4", objEmpresa.codac4);
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);


            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }


        public clsError f_Empresa_Eliminar(string s_rucemp)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_EMPRESA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Delete");
            cmd.Parameters.AddWithValue("@RUCEMP", s_rucemp);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }




        public cls_seg_Empresa f_dtEmpresaToObjEmpresa(DataTable dt) //para pasar de dt a objUsuario los atributos de la base
        {
            cls_seg_Empresa objEmpresa = new cls_seg_Empresa();
            objEmpresa.Error = new clsError();
            objEmpresa.dtEmpresa = dt;
            objEmpresa.rucemp = dt.Rows[0].Field<string>("rucemp");
            objEmpresa.nomemp = dt.Rows[0].Field<string>("nomemp");
            objEmpresa.estcad = dt.Rows[0].Field<string>("estcad");
            objEmpresa.feccad = dt.Rows[0].Field<DateTime>("feccad");
            objEmpresa.numlic = dt.Rows[0].Field<string>("numlic");
            objEmpresa.serlic = dt.Rows[0].Field<string>("serlic");
            objEmpresa.espdis = dt.Rows[0].Field<Decimal>("espdis");
            objEmpresa.codac1 = dt.Rows[0].Field<string>("codac1");
            objEmpresa.codac2 = dt.Rows[0].Field<string>("codac2");
            objEmpresa.codac3 = dt.Rows[0].Field<string>("codac3");
            objEmpresa.codac4 = dt.Rows[0].Field<string>("codac4");

            return objEmpresa;
        }
        public clsError f_Empresa_Encriptar_Base(cls_seg_Empresa objEmpresa, string s_palcla)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[SEG_M_EMPRESA_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@RucEmp", objEmpresa.rucemp);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);
            cmd.Parameters.AddWithValue("@NomDbm", objEmpresa.nomdbm);
            cmd.Parameters.AddWithValue("@NomBas", objEmpresa.nombas);
            cmd.Parameters.AddWithValue("@UsuBas", objEmpresa.usubas); //campo para Update
            cmd.Parameters.AddWithValue("@ClaBas", objEmpresa.clabas);
            cmd.Parameters.AddWithValue("@IpServ", objEmpresa.ipserv);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public cls_seg_Empresa f_Empresa_Desencriptar_Base(cls_seg_Empresa objEmpresa, string s_palcla)
        {

            objEmpresa.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[SEG_V_EMPRESA_BASE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RucEmp", objEmpresa.rucemp);
            cmd.Parameters.AddWithValue("@PalCla", s_palcla);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                //DataSet ds = new DataSet();
                //sda.Fill(ds);
                //objUsuario = f_dtUsuarioToObjUsuario(ds.Tables[0]);
                objEmpresa.nomdbm = dt.Rows[0].Field<string>("nomdbm");
                objEmpresa.nombas = dt.Rows[0].Field<string>("nombas");
                objEmpresa.usubas = dt.Rows[0].Field<string>("usubas");
                objEmpresa.clabas = dt.Rows[0].Field<string>("clabas");
                objEmpresa.ipserv = dt.Rows[0].Field<string>("ipserv");
            }
            catch (Exception ex)
            {
                objEmpresa.Error = objEmpresa.Error.f_ErrorControlado(ex);
            }
            conn.Close();
            return objEmpresa;
        }


        public clsError f_Empresa_Auditoria(string s_rucemp, string s_usuing, DateTime? s_fecing, string s_codusu, DateTime? s_fecult)
        {
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE [seg_empresa] SET [usuing] = @Usuing, [fecing] = @Fecing, [codusu] = @Codusu, [fecult] = @Fecult WHERE [rucemp] = @Rucemp", conn);
            cmd.Parameters.AddWithValue("@Usuing", s_usuing);
            cmd.Parameters.AddWithValue("@Fecing", s_fecing);
            cmd.Parameters.AddWithValue("@Codusu", s_codusu);
            cmd.Parameters.AddWithValue("@Fecult", s_fecult);
            cmd.Parameters.AddWithValue("@Rucemp", s_rucemp);


            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
        public string f_ValidacionCedula(string s_codcli, string s_tipemp)
        {
            //string s_codcli = "171939445905";
            string s_CodAsi, s_PriApe, s_SegApe, s_PriNom, s_SegNom, s_CodCli, s_TipEmp;
            s_TipEmp = s_tipemp;
            double l_Fila;
            string s_CedIde, s_Aux;
            double[] r_ArrCed = new double[9];
            double[] r_AuxArr = new double[9];
            double[] r_ArrCe2 = new double[8];
            double[] r_AuxAr2 = new double[8];
            double r_Valor, r_ValDig;

            if (!(s_codcli.Trim().Length == 10 || s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 9))
            {
                return "Solo se puede ingresar RUC = 13 Digitos, CI=10 Digitos, PASAPORTE=9 Digitos";
            }

            if (s_codcli.Trim().Length == 13) //RUC
            {

                if (s_codcli.Substring(12, 1) != "1")
                {
                    return "RUC el digito 13 debe ser 1";
                }

            }

            r_AuxArr[0] = 2;
            r_AuxArr[1] = 1;
            r_AuxArr[2] = 2;
            r_AuxArr[3] = 1;
            r_AuxArr[4] = 2;
            r_AuxArr[5] = 1;
            r_AuxArr[6] = 2;
            r_AuxArr[7] = 1;
            r_AuxArr[8] = 2;

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            r_Valor = 0;
            if (s_codcli.Trim().Length == 13 || s_codcli.Trim().Length == 10) //RUC o CEDULA
            {
                if (s_TipEmp == "1")
                {
                    r_AuxArr[0] = 4;
                    r_AuxArr[1] = 3;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 7;
                    r_AuxArr[4] = 6;
                    r_AuxArr[5] = 5;
                    r_AuxArr[6] = 4;
                    r_AuxArr[7] = 3;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));


                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Digito Autoverificador Erroneo en el Código";
                    }
                }
                if (s_TipEmp == "2")
                {
                    r_AuxAr2[0] = 3;
                    r_AuxAr2[1] = 2;
                    r_AuxAr2[2] = 7;
                    r_AuxAr2[3] = 6;
                    r_AuxAr2[4] = 5;
                    r_AuxAr2[5] = 4;
                    r_AuxAr2[6] = 3;
                    r_AuxAr2[7] = 2;
                    r_ArrCe2[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCe2[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCe2[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCe2[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCe2[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCe2[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCe2[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCe2[7] = double.Parse(s_codcli.Substring(7, 1));

                    for (int i = 0; i <= 7; i++)
                    {
                        r_ValDig = r_ArrCe2[i] * r_AuxAr2[i];
                        //if (r_ValDig > 9)
                        //    r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 11;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 11 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Digito Autoverificador Erroneo en el Código";
                    }
                }

                if (s_TipEmp == "3")
                {
                    r_AuxArr[0] = 2;
                    r_AuxArr[1] = 1;
                    r_AuxArr[2] = 2;
                    r_AuxArr[3] = 1;
                    r_AuxArr[4] = 2;
                    r_AuxArr[5] = 1;
                    r_AuxArr[6] = 2;
                    r_AuxArr[7] = 1;
                    r_AuxArr[8] = 2;
                    r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
                    r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
                    r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
                    r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
                    r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
                    r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
                    r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
                    r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
                    r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

                    for (int i = 0; i <= 8; i++)
                    {
                        r_ValDig = r_ArrCed[i] * r_AuxArr[i];
                        if (r_ValDig > 9)
                            r_ValDig = r_ValDig - 9;
                        r_Valor = r_Valor + r_ValDig;
                    }
                    r_Valor = r_Valor % 10;    //CREO DEVUELVE EL RESIDUO
                    if (r_Valor != 0)
                        r_Valor = 10 - r_Valor;
                    if (double.Parse(s_codcli.Substring(9, 1)) != r_Valor)
                    {
                        return "Digito Autoverificador Erroneo en el Código";
                    }
                }
            }

            r_ArrCed[0] = double.Parse(s_codcli.Substring(0, 1));
            r_ArrCed[1] = double.Parse(s_codcli.Substring(1, 1));
            r_ArrCed[2] = double.Parse(s_codcli.Substring(2, 1));
            r_ArrCed[3] = double.Parse(s_codcli.Substring(3, 1));
            r_ArrCed[4] = double.Parse(s_codcli.Substring(4, 1));
            r_ArrCed[5] = double.Parse(s_codcli.Substring(5, 1));
            r_ArrCed[6] = double.Parse(s_codcli.Substring(6, 1));
            r_ArrCed[7] = double.Parse(s_codcli.Substring(7, 1));
            r_ArrCed[8] = double.Parse(s_codcli.Substring(8, 1));

            return "CORRECTO"; //SI NO HAY ERROR
        }


        public string f_DeterminarTipInd(string s_tipind)
        {
            if (s_tipind.Equals("1"))
            {
                s_tipind = "RUC";
            }
            else
            {
                if (s_tipind.Equals("2"))
                {
                    s_tipind = "Cédula";
                }
                else
                {
                    if (s_tipind.Equals("3"))
                    {
                        s_tipind = "Pasaporte";
                    }
                    else
                    {
                        s_tipind = "-";
                    }
                }
            }

            return s_tipind;
        }

        public string f_DeterminarTipEmp(string s_tipemp)
        {
            if (s_tipemp.Equals("1"))
            {
                s_tipemp = "Privada";
            }
            else
            {
                if (s_tipemp.Equals("2"))
                {
                    s_tipemp = "Pública";
                }
                else
                {
                    if (s_tipemp.Equals("3"))
                    {
                        s_tipemp = "Natural";
                    }
                    else
                    {
                        s_tipemp = "-";
                    }

                }
            }

            return s_tipemp;
        }

        public clsRootObjectRUC f_ObtenerDatosRUC(string s_ruc)
        {
            //string s_ruc = "0702061730001";
            string s_url = "http://gextionix.net:8082/api/sri?ruc=" + s_ruc;
            return f_download_serialized_json_data<clsRootObjectRUC>(s_url);

        }

        public clsRootObjectCedula f_ObtenerDatosCED(string s_cedula)
        {
            //string s_ruc = "0702061730001";
            string s_url = "http://gextionix.net:8082/api/registrocivil?cedula=" + s_cedula;
            return f_download_serialized_json_data<clsRootObjectCedula>(s_url);

        }

        private static T f_download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }


    }

}