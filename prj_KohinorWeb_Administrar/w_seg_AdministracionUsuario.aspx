﻿<%@ Page Title="Usuario" Language="C#" MasterPageFile="~/w_Principal.Master" AutoEventWireup="true" CodeBehind="w_seg_AdministracionUsuario.aspx.cs" Inherits="prj_KohinorWeb_Administrar.w_seg_AdministracionUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Kohinor Admin - Usuario</title>
	<script type="text/javascript">
		function f_ModalUsuario() {
			$("#btnShowUsuarioModal").click();
		}
	</script>
</asp:Content>
<asp:Content ID="ContentBodyHeader" ContentPlaceHolderID="cphBodyHeader" runat="server">
	<div class="c-subheader justify-content-between px-3">
		<!-- Breadcrumb-->
		<ol class="breadcrumb border-0 m-0">
			<%--<li class="breadcrumb-item">
				<asp:Label ID="lblModuloActivo" runat="server"></asp:Label></li>--%>
			<li class="breadcrumb-item active"><h5>Administración de Usuarios</h5></li>
			<!-- Breadcrumb Menu-->
		</ol>
		<div class="c-subheader-nav d-md-down-none mfe-2">
			<div id="spinner" class="spinner-border" role="status">
				<span class="sr-only">Loading...</span>
			</div>
            <%--<a class="c-subheader-nav-link">
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn btn-primary" type="button" id="">
							<i class="cil-filter"></i>
						</button>
					</div>

					<asp:TextBox ID="txtSearch" runat="server" placeholder="Buscar..." class="form-control mr-sm-2"></asp:TextBox>
				</div>
				<asp:LinkButton ID="lkbtnSearch" runat="server" class="btn btn-outline-primary my-2 my-sm-0 "> <span class="cil-search"></span></asp:LinkButton>
			</a>--%>
		</div>
	</div>
	<div class="c-subheader px-3">		
		<ol class="breadcrumb border-0 m-0">
			<li class="breadcrumb-item active">
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-primary" id="btnNuevo" runat="server" onserverclick="btnNuevo_Click" formnovalidate><i class="cil-file"></i>Nuevo</button>
					<button type="button" class="btn btn-outline-dark" id="btnAbrir" runat="server" data-toggle="modal" data-target="#UsuarioModal"><i class="cil-folder-open"></i>Abrir</button>
					<button type="button" class="btn btn-outline-dark" id="btnGuardar" runat="server" onserverclick="btnGuardarUsuario_Click"><i class="cil-save"></i>Guardar</button>
					<button type="button" class="btn btn-outline-dark" id="btnCancelar" runat="server" onserverclick="btnCancelar_Click" formnovalidate><i class="cil-grid-slash"></i>Cancelar</button>
					<button type="button" class="btn btn-outline-danger" id="btnEliminar" runat="server" onserverclick="lkbtnDelete_Click" formnovalidate><i class="cil-trash"></i>Eliminar</button>
				</div>
				<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-backward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-skip-forward"></i></button>
					<button type="button" class="btn btn-outline-secondary disabled"><i class="cil-media-step-forward"></i></button>
				</div>

				<asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green" align="right"></asp:Label>
				<asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon" align="right"></asp:Label>
			</li>
		</ol>

	</div>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="cphBody" runat="server">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <%--<i class="c-icon cil-justify-center"></i>--%>
                                    <h4><b>Cuenta de Registro</b></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">RUC Empresa</label>--%>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>RUC Empresa</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtRucemp" class="form-control form-control-user" placeholder="RUC Empresa" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">Cuenta Usuario</label>--%>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Cuenta Usuario</strong></span>
                                                    </div>
                                                    <input type="text" class="form-control form-control-user" id="txtCodus1" aria-describedby="emailHelp" placeholder="Cuenta Usuario" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">Nombre Usuario</label>--%>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Nombre Usuario</strong></span>
                                                    </div>
                                                    <input type="text" class="form-control form-control-user" id="txtNomusu" aria-describedby="emailHelp" placeholder="Nombre Usuario" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">Nombre Usuario</label>--%>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Clave Usuario</strong></span>
                                                    </div>
                                                    <div class="input-group-text">
                                                        <asp:CheckBox ID="chkClausu" runat="server" />
                                                    </div>
                                                    <input type="text" class="form-control form-control-user" id="txtClausu" aria-describedby="emailHelp" placeholder="Clave Usuario" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">Email</label>--%>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Email</strong></span>
                                                    </div>
                                                    <input type="text" class="form-control form-control-user" id="txtEmail" aria-describedby="emailHelp" placeholder="Email" runat="server" />
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                </div>
                            </div>
                            <div class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <%--<i class="c-icon cil-justify-center"></i>--%>
                                    <h4><b>Datos Conexión</b></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>DBMS</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtNomdbm" class="form-control form-control-user" placeholder="Nomdbm" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Base</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtNombas" class="form-control form-control-user" placeholder="Nombas" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Usuario</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtUsubas" class="form-control form-control-user" placeholder="Usubas" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Clave</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtClabas" class="form-control form-control-user" placeholder="Clabas" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>IP Servidor</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtIpserv" class="form-control form-control-user" placeholder="Ipserv" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                </div>
                            </div>

                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6">
                            <div class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <%--<i class="c-icon cil-justify-center"></i>--%>
                                    <h4><b>Datos Equipo a Registrar</b></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Equipo a Registrar</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtPcnom" class="form-control form-control-user" placeholder="Nombre PC" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Dirección IP</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtPcip" class="form-control form-control-user" placeholder="PC IP" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Dirección MAC</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtPcmac" class="form-control form-control-user" placeholder="PC MAC" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Estado Usuario</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtEstusu" class="form-control form-control-user" placeholder="" Text="1" runat="server" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label">Última Entrada</label>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtEntrada" class="form-control form-control-user" runat="server" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label">Última Salida</label>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtSalida" class="form-control form-control-user" runat="server" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <!-- /.row-->
                                </div>
                            </div>
                            <div class="card bg-gradient-secondary">
                                <div class="card-header">
                                    <%--<i class="c-icon cil-justify-center"></i>--%>
                                    <h4><b>Datos de Activación</b></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Estado</strong></span>
                                                    </div>
                                                    <asp:DropDownList ID="ddlEstado" runat="server" class="form-control">
                                                        <asp:ListItem Value="0">Pasivo</asp:ListItem>
                                                        <asp:ListItem Value="1">Activo</asp:ListItem>
                                                        <asp:ListItem Value="2">En proceso</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Expiración</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtFecexp" class="form-control form-control-user" runat="server" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<label for="name">Cuenta Usuario</label>--%>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Tipo Acceso</strong></span>
                                                    </div>
                                                    <asp:DropDownList ID="ddlTipacc" runat="server" class="form-control">
                                                        <asp:ListItem Value="0">Supervisor/Admin</asp:ListItem>
                                                        <asp:ListItem Value="1">Administrador</asp:ListItem>
                                                        <asp:ListItem Value="2">Usuario</asp:ListItem>
                                                        <asp:ListItem Value="3">Cliente</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Serie</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtSerie" class="form-control form-control-user" placeholder="Serie" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <strong>Sucursal</strong></span>
                                                    </div>
                                                    <asp:TextBox ID="txtSucursal" class="form-control form-control-user" placeholder="Sucursal" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row-->
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- /.row-->
                </div>
            </div>
        </div>
    </main>

    <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsUsuario" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>"
        SelectCommand="SELECT [rucemp], [codus1], [nomusu], [clausu], [codsuc], [sersec], [usmail], [estado],[estusu], [tipacc], [pcnom], [pcmac], [pcip] from [seg_usuario]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsPalabraClave" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [palcla] FROM [seg_configuracion]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsUsuarioExiste" runat="server" ConnectionString="<%$ ConnectionStrings:SQLCA %>" SelectCommand="SELECT [rucemp], [codus1] FROM [seg_usuario] WHERE (([rucemp] = @rucemp) AND ([codus1] = @codus1))">
        <SelectParameters>
            <asp:SessionParameter Name="rucemp" SessionField="gs_Rucemp" Type="String" />
            <asp:SessionParameter Name="codus1" SessionField="gs_Codus1" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--f SQL DATASOURCES-->

    <!-- Modal grvUsuario -->
    <button id="btnShowUsuarioModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#UsuarioModal"></button>
    <div class="modal fade" id="UsuarioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Usuario</h4>
                    <button class="close" id="btnCloseUsuarioModal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-row">

                        <div class="form-group col-md-12">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="lkbtnSearchUsuario" runat="server" OnClick="f_GridBuscar" class="btn btn-primary"> <span class="cil-search"></span></asp:LinkButton>
                                </div>
                                <asp:TextBox ID="txtSearchUsuario" runat="server" placeholder="Buscar..." class="form-control mr-sm-2" Width="50%"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group col-md-12">

                            <div class="table table-responsive" style="overflow-x: auto;">
                                <div class="thead-dark">
                                    <asp:UpdatePanel ID="updUsuario" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <asp:GridView ID="grvUsuario" class="table table-bordered table-active table-active table-hover table-striped"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="grvUsuario_RowDataBound" OnRowCreated="grvUsuario_RowCreated" OnSelectedIndexChanged="grvUsuario_SelectedIndexChanged"
                                                AllowSorting="true" OnSorting="OnSorting" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10">
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updUsuarioSelect" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnSelectUsuario" runat="server" CommandName="Select" ImageUrl="~/Icon/Menu125/Select-Hand.png" Width="30px" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSelectUsuario" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="rucemp" HeaderText="RUC" SortExpression="rucemp" />
                                                    <asp:BoundField DataField="codus1" HeaderText="Código" SortExpression="codus1" />
                                                    <asp:BoundField DataField="nomusu" HeaderText="Nombre" SortExpression="nomusu" />
                                                    <asp:BoundField DataField="codsuc" HeaderText="Sucursal" SortExpression="codsuc" />
                                                    <asp:BoundField DataField="sersec" HeaderText="Serie" SortExpression="sersec" />
                                                    <asp:BoundField DataField="usmail" HeaderText="Email" SortExpression="usmail" />
                                                    <asp:BoundField DataField="pcmac" HeaderText="IP" SortExpression="pcip" />
                                                    <asp:BoundField DataField="estado" HeaderText="Estado" SortExpression="estado" />

                                                </Columns>
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="pagination-ys justify-content-center align-items-center" />
                                            </asp:GridView>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Eliminar -->
    <button id="btnShowWarningModal" class="btn btn-danger mb-1" style="display: none;" type="button" data-toggle="modal" data-target="#warningModal">Eliminar modal</button>
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Usuario</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea eliminar el Usuario '<asp:Label ID="lblEliUsu" runat="server" Font-Bold="True"></asp:Label>' ?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-danger" type="button" runat="server" onserverclick="btnEliminarFactura_Click">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>

    <!-- Modal Danger (Error) -->
    <div class="modal fade" id="ModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">N° Error</span>
                        </div>
                        <asp:TextBox ID="txtModalNum" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>

                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Mensaje</h4>
                        <asp:Label ID="lblModalFecha" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="lblModalError" runat="server" Text="Label" ForeColor="Maroon"></asp:Label>
                        </p>
                        <hr>
                        <p class="mb-0">
                            <strong>Donde: </strong>
                            <asp:Label ID="lblModalPagina" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </p>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Objeto</span>
                        </div>
                        <asp:TextBox ID="txtModalObjeto" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Evento</span>
                        </div>
                        <asp:TextBox ID="txtModalEvento" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Línea</span>
                        </div>
                        <asp:TextBox ID="txtModalLinea" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger" type="button">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
    </div>
</asp:Content>


